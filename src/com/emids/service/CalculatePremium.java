package com.emids.service;

import com.emids.client.Client;

public interface CalculatePremium {
	
	public double getPremiumAmount(Client client);
	
	public double totalPercentByAgeLimit(int age);
	
	public double amountByGender(String gender, double amount);
	
	public double getCostForHealthStatus(Client client, double amount);

}
