package com.emids.service.impl;

import com.emids.client.Client;
import com.emids.common.Constants;
import com.emids.health.rules.HealthRules;
import com.emids.service.CalculatePremium;

public class CalculatePremiumImpl implements CalculatePremium {

	private HealthRules util;

	public CalculatePremiumImpl(HealthRules util) {
		super();
		this.util = util;
	}

	@Override
	public double getPremiumAmount(Client client) {

		double amountToBePaid = 0;
		// condition based on age
		amountToBePaid = totalPercentByAgeLimit(client.getAge());
		// condition based on gender
		amountToBePaid = amountByGender(client.getGender(), amountToBePaid);
		// condition based on Health and Habits
		amountToBePaid = getCostForHealthStatus(client, amountToBePaid);
		return amountToBePaid;
	}

	@Override
	public double totalPercentByAgeLimit(int age) {

		double amount = 0;
		amount = age <= 40 ? util.calculatePoint(age) : util
				.calculatePoint(age) + (util.calculatePoint(age) * 0.2);
		return amount;
	}

	@Override
	public double amountByGender(String gender, double amount) {

		double amountByAge = 0;
		if (gender.equalsIgnoreCase(Constants.MALE)
				|| gender.equalsIgnoreCase(Constants.FEMALE)) {
			amountByAge = amount + (amount * 0.02);
		}
		return amountByAge;

	}

	@Override
	public double getCostForHealthStatus(Client client,
			double amount) {

		if (client.getBloodPressure().equalsIgnoreCase(Constants.YES)) {
			amount = amount + (amount * 0.01);
		}
		if (client.getBloodSugar().equalsIgnoreCase(Constants.YES)) {
			amount = amount + (amount * 0.01);
		}
		if (client.getHypertension().equalsIgnoreCase(Constants.YES)) {
			amount = amount + (amount * 0.01);
		}
		if (client.getOverWeight().equalsIgnoreCase(Constants.YES)) {
			amount = amount + (amount * 0.01);
		}
		if (client.getAlcohol().equalsIgnoreCase(Constants.YES)) {
			amount = amount + (amount * 0.03);
		}
		if (client.getDrugs().equalsIgnoreCase(Constants.YES)) {
			amount = amount + (amount * 0.03);
		}
		if (client.getSmoking().equalsIgnoreCase(Constants.YES)) {
			amount = amount + (amount * 0.03);
		}
		if (client.getDailyExercise().equalsIgnoreCase(Constants.YES)) {
			amount = amount - (amount * 0.03);
		}
		return amount;
	}

}
