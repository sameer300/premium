package com.emids.common;

public class Constants {
	
	// static constant for insurance 
	public final static String YES = "Yes";
	public final static String NO = "No";
	public final static String MALE = "Male";
	public final static String FEMALE = "Female";
	public final static double MIN_PREMIUM_AMOUNT = 5000;

}
