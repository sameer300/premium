package com.emids.main;

import java.util.Scanner;

import com.emids.client.Client;
import com.emids.health.rules.HealthRules;
import com.emids.response.Response;
import com.emids.service.impl.CalculatePremiumImpl;

public class PremiumGeneratorMain {

	static double totalPremiumAmount = 0;
	private static CalculatePremiumImpl calculatePremium;
	
	private static Response response;
	
	public static void main(String[] args) {
		
		Client client = loadData();
		
		totalPremiumAmount = calculatePremium.getPremiumAmount(client);
		response.setAmount(totalPremiumAmount);
		response.setName(client.getName());
		System.out.println("Health Insurance Premium for Mr. Gomes:"+totalPremiumAmount);

	}

	
	private static Client loadData(){
		
		
		Scanner scanner = new Scanner(System.in);
		Client client = new Client();
		HealthRules rules = new HealthRules();
		calculatePremium = new CalculatePremiumImpl(rules);
		response = new Response();
		
		System.out.print("Enter Client name: ");
		
		String username = scanner.next();
		
		System.out.print("Enter Client Age: ");
		
		int age = scanner.nextInt();
		
		
		System.out.print("Gender: ");
		
		String gender = scanner.next();
		
		System.out.print("Hypertension: ");
		String hypertension = scanner.next();
		
		
		System.out.print("Blood pressure: ");
		String bloodPressure = scanner.next();
		
		System.out.print("Blood Sugur: ");
		String bloodSugar = scanner.next();
		
		System.out.print("Over Weight: ");
		String overWeight = scanner.next();
		
		System.out.print("Smoking: ");
		String smoking = scanner.next();
		
		System.out.print("Alcohol: ");
		String alcohol = scanner.next();
		
		System.out.print("Daily Exsersice: ");
		String dailyExercise = scanner.next();
		
		System.out.print("Drugs: ");
		String drugs = scanner.next();
		
		client.setName(username);
		client.setAge(age);
		client.setAlcohol(alcohol);
		client.setBloodPressure(bloodPressure);
		client.setBloodSugar(bloodSugar);
		client.setDailyExercise(dailyExercise);
		client.setDrugs(drugs);
		client.setHypertension(hypertension);
		client.setOverWeight(overWeight);
		client.setGender(gender);
		client.setSmoking(smoking);
		
		
		
		return client;
		
		
	}

}
