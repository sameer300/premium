package com.emid.main.test;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.emids.client.Client;
import com.emids.health.rules.HealthRules;
import com.emids.response.Response;
import com.emids.service.impl.CalculatePremiumImpl;

 
public class PremiumGeneratorMainTest {

	@Mock
	HealthRules rules;

	@Mock
	CalculatePremiumImpl calculatePremium;

	@Mock
	Response response;

	private Object daoMock;

	Client client = new Client();

	@Before
	public void setUp() throws Exception {

		MockitoAnnotations.initMocks(this);

		client.setName("Sameer Mujawar");
		client.setAge(34);
		client.setBloodPressure("No");
		client.setBloodSugar("No");
		client.setDailyExercise("Yes");
		client.setDrugs("No");
		client.setGender("Male");
		client.setSmoking("No");
		client.setHypertension("No");
		client.setOverWeight("Yes");
		client.setAlcohol("No");

	}

	@Test
	public void testMain() {

		setDaoMock(calculatePremium.getPremiumAmount(client));

	}

	public Object getDaoMock() {
		return daoMock;
	}

	public void setDaoMock(Object daoMock) {
		this.daoMock = daoMock;
	}

	

}
