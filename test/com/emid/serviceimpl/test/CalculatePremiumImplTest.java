package com.emid.serviceimpl.test;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import com.emids.client.Client;
import com.emids.common.Constants;
import com.emids.health.rules.HealthRules;

public class CalculatePremiumImplTest {

	@Mock
	Client client;

	@Mock
	HealthRules util;
	
	private Object daoMock;
	
	

	@Before
	public void setUp() throws Exception {
		util = new HealthRules();
		client = new Client();

		client.setName("Sameer Mujawar");
		client.setAge(34);
		client.setBloodPressure("No");
		client.setBloodSugar("No");
		client.setDailyExercise("Yes");
		client.setDrugs("No");
		client.setGender("Male");
		client.setSmoking("No");
		client.setHypertension("No");
		client.setOverWeight("Yes");
		client.setAlcohol("No");
	}

	@Test
	public void testCalculatePremiumImpl() {

	}

	@Test
	public void testGetPremiumAmount() {
		@SuppressWarnings("unused")
		double amountToBePaid = 6510.00;
		// condition based on age
		testTotalPercentByAgeLimit();
		// condition based on gender
		testAmountByGender();
		// condition based on Health and Habits
		testGetCostForHealthStatus();
		return;
	}

	@Test
	public void testTotalPercentByAgeLimit() {

		daoMock = client.getAge() <= 40 ? util.calculatePoint(client.getAge()) : util
				.calculatePoint(client.getAge()) + (util.calculatePoint(client.getAge()) * 0.2);
		return;
	}

	@Test
	public void testAmountByGender() {
		double amount = 5100.00;
		if (client.getGender().equalsIgnoreCase(Constants.MALE)
				|| client.getGender().equalsIgnoreCase(Constants.FEMALE)) {
			double amountByAge = amount + (amount * 0.02);
		}
		return;
	}

	@Test
	public void testGetCostForHealthStatus() {
		double amount = 5100.00;
		if (client.getBloodPressure().equalsIgnoreCase(Constants.YES)) {
			amount = amount + (amount * 0.01);
		}
		if (client.getBloodSugar().equalsIgnoreCase(Constants.YES)) {
			amount = amount + (amount * 0.01);
		}
		if (client.getHypertension().equalsIgnoreCase(Constants.YES)) {
			amount = amount + (amount * 0.01);
		}
		if (client.getOverWeight().equalsIgnoreCase(Constants.YES)) {
			amount = amount + (amount * 0.01);
		}
		if (client.getAlcohol().equalsIgnoreCase(Constants.YES)) {
			amount = amount + (amount * 0.03);
		}
		if (client.getDrugs().equalsIgnoreCase(Constants.YES)) {
			amount = amount + (amount * 0.03);
		}
		if (client.getSmoking().equalsIgnoreCase(Constants.YES)) {
			amount = amount + (amount * 0.03);
		}
		if (client.getDailyExercise().equalsIgnoreCase(Constants.YES)) {
			amount = amount - (amount * 0.03);
		}
		return;
	}
	
	
	public Object getDaoMock() {
		return daoMock;
	}

	public void setDaoMock(Object daoMock) {
		this.daoMock = daoMock;
	}

}
